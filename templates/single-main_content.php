<div class="container">

	<div class="inner-container">

		<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

			<?php $fa_icon = get_post_meta( $post->ID, 'fontawesome_icon', true ); ?>

			<article>
				
				<h2>
					<i class="fa <?php echo $fa_icon; ?>" aria-hidden="true"></i>&nbsp;
					<?php the_title(); ?>
				</h2>
				
				<?php the_content(); ?>


				
			</article>
			
			<div class="next-article">
			 	<?php previous_post_link('Próximo artículo: %link &raquo;'); ?>
			</div>

		<?php endwhile; endif; ?>


	</div>