<div class="contact-button-modal">                
    <a href="mailto:juanmtorrijos@gmail.com" class="contact-button">
        Contáctame
    </a>
    <div class="modal-transformer">
		<div class="contact-us-modal">
			<div class="close-modal">X</div>
			<form class="contact-form-modal">
				<h3>Contáctame</h3> 
				<div class="contact-form-text">
					<p>
						<label>Nombre:</label>
						<input type="text" placeholder="Tu nombre aquí ">
					</p>
					<p>
						<label>Correo:</label> 
						<input type="email" placeholder="Tu email aquí ">
					</p>
					<p>
						<label>Teléfono:</label>
						<input type="text" placeholder="Tu celular aquí ">
					</p>
					<p>
						<label>Mensaje:</label>
						<textarea 
							name="asunto" 
							id="moitorrijos_asunto" 
							placeholder="Estoy interesad@ en una página o aplicación Web..."></textarea>
					</p>
				</div>
				<button type="submit">
					Enviar
					<div class="ball-pulse-sync">
						<div></div>
						<div></div>
						<div></div>
					</div>
				</button>
				
			</form>

			<div class="thank-you-message">
				<h3>Gracias</h3>
				<p>Gracias por escribirme, revisa tu correo para una sorpresa. pronto te estaré contactando...</p>
			</div>
		</div>
    	
    </div>
</div>

