<div class="container">

	<div class="inner-container">

	<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

		<?php $fa_icon = get_post_meta( $post->ID, 'fontawesome_icon', true ); ?>

		<article>
			
			<h2>

				<a href="<?php the_permalink(); ?>">
					<i class="fa <?php echo $fa_icon; ?>" aria-hidden="true"></i>&nbsp;
					<?php the_title(); ?>
				</a>
			</h2>
			
			<?php the_excerpt(); ?>
			
		</article>

	<?php endwhile; else: ?>

		<article>
					
			<h2>
				<i class="fa fa-coffee" aria-hidden="true"></i>&nbsp;
				Another Title Here
			</h2>
			
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
			tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
			quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
			consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
			cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
			proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
			
		</article>

	<?php endif; ?>


	</div>

