<?php

define ( 'THEMEROOT', get_template_directory_uri() );
define ( 'IMAGESPATH', THEMEROOT . '/images' );

require get_template_directory() . '/includes/register_my_styles_and_scripts.php';
require get_template_directory() . '/includes/wp-title-hack-for-home.php';
require get_template_directory() . '/includes/new_excerpt_more.php';
require get_template_directory() . '/includes/theme-favicon.php';
require get_template_directory() . '/includes/google-fonts.php';
require get_template_directory() . '/includes/html5shimie9.php';
require get_template_directory() . '/includes/js-class-body.php';
require get_template_directory() . '/includes/post-thumb.php';
require get_template_directory() . '/includes/register-sidebars.php';
require get_template_directory() . '/includes/register-menus.php';
require get_template_directory() . '/includes/add-local-remote-button-admin-bar.php';
