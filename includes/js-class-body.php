<?php

/**
 * Prints a js script to the head that adds 'js' class to body tag if javascript is enabled
 * 
 */
function add_js_class_body() {
	echo '<script>document.documentElement.className = " js"</script>';
}
add_action ('wp_head', 'add_js_class_body');