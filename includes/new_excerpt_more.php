<?php
/**
 * Create read more link for blog page
 * 
 * @param  string
 * @return string
 *
 */
function new_excerpt_more( $more ) {
  return '... <a class="read-more" href="'. get_permalink( get_the_ID() ) . '">' . __('Lea más &rarr;', 'intermaritime_theme') . '</a>';
}
add_filter( 'excerpt_more', 'new_excerpt_more' );

/**
 * Change excerpt length
 * 
 * @param  string
 * @return string
 *
 */
function custom_excerpt_length( $length ) {
	return 60;
}
add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );