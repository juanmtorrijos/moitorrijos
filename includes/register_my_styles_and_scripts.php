<?php
/**
 * Register styles and scripts the WordPress way
 */

function register_my_styles_and_scripts() {
	
	wp_register_style( 'main_style', THEMEROOT . '/css/main.css', array(), '2016', 'all' );

	wp_enqueue_style( 'main_style' );

	wp_enqueue_script( 'velocity_js', THEMEROOT . '/js/velocity.js', array(), '1.3.1', true );

	if ( is_home() || is_front_page() ) {

	 	wp_enqueue_script( 'main_js', THEMEROOT . '/js/main.js', array(), '2016', true );
		wp_enqueue_script( 'contact_js', THEMEROOT . '/js/open-modal-animation.js', array(), '2016', true );
		wp_enqueue_script( 'send_form_js', THEMEROOT . '/js/send-form.js', array(), '2016', true );
		wp_enqueue_script( 'close_contact_modal_js', THEMEROOT . '/js/close-modal-animation.js', array(), '2016', true );

	} else {
		wp_enqueue_script( 'single_js', THEMEROOT . '/js/single.js', array(), '2016', true );
	}


}

add_action( 'wp_enqueue_scripts', 'register_my_styles_and_scripts' );