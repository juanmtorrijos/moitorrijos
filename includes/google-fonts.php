<?php

/**
 * Prints Google fonts link to the head
 */
function register_google_fonts() {
	echo '
	<link href="https://fonts.googleapis.com/css?family=Inconsolata|Oswald|Raleway" rel="stylesheet">
	';
}

add_action('wp_head', 'register_google_fonts');