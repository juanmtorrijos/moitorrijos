;(function(){

var contactButton = document.querySelector('a.contact-button'),
	modalTransformer = document.querySelector('.modal-transformer'),
	contactModal = document.querySelector('.contact-us-modal'),
	thankYouMessage = document.querySelector('.thank-you-message'),
	formModal = document.querySelector('form.contact-form-modal'),
	closeModal = document.querySelector('.close-modal');

closeModal.addEventListener('click', function(){
	contactButton.style.opacity = 1;
	Velocity(formModal, {opacity: 0}, {duration: 100});
	Velocity(thankYouMessage, {opacity: 0}, {duration: 100});
	setTimeout(function(){
		Velocity(contactModal, {opacity: 0}, {visibility: 'hidden'}, {duration: 50});
	}, 150);
	setTimeout(function(){
		Velocity(modalTransformer, 
			{
				width: 200, 
				height: 47,
				left: 0,
				top: 0,

			}, {duration: 300});
		formModal.style.visibility = 'hidden';
	}, 350);
	setTimeout(function(){
		Velocity(modalTransformer, {opacity: 0}, {duration: 200});
		modalTransformer.style.visibility = 'hidden';
	}, 600);
});

})();