;(function(){

var bannerText = document.querySelector('.banner-text'),
	innerBanner = document.querySelector('.inner-banner'),
	moiPicture = document.querySelector('.moitorrijos-image'),
	innerContainer = document.querySelector('.inner-container');

Velocity(bannerText, 
	{
		opacity: 1,
		translateY: ((innerBanner.offsetHeight - bannerText.offsetHeight)/2) + 'px'
	}, { duration: 600 }
);

window.addEventListener('load', function(){
	Velocity( moiPicture, { opacity: 1}, { duration: 600 });
	Velocity( innerContainer, { translateY: '-60px' }, { duration: 600 });
});

})();