;(function(){

var contactButton = document.querySelector('a.contact-button'),
	modalTransformer = document.querySelector('.modal-transformer'),
	contactModal = document.querySelector('.contact-us-modal'),
	formModal = document.querySelector('form.contact-form-modal'),
	innerBanner = document.querySelector('.inner-banner'),
	closeModal = document.querySelector('.close-modal');

var windowWidth = window.innerWidth;

if ( windowWidth >= 768 ) {

	contactButton.addEventListener('click', function( event ){
		event.preventDefault();
		Velocity(modalTransformer, 
			{opacity: 1},
			{visibility: 'visible'},
			{duration: 50}
		);
		setTimeout(function(){
			Velocity(modalTransformer, 
				{
					backgroundColor: '#172fa9',
				},
				{duration: 150}
			);
		}, 150);
		setTimeout(function(){
			Velocity(modalTransformer, 
				{
					width: 600,
					height: 423,
					top: -350,
					left: -140
				},
				{duration: 150}
			);
		}, 400);
		setTimeout(function(){
			Velocity(contactModal, 
				{opacity: 1}, 
				{visibility: 'visible'}, 
				{duration: 150});
		}, 600);
		setTimeout(function(){
			Velocity(formModal, 
				{opacity: 1}, 
				{visibility: 'visible'}
			);
		}, 800);
	});
}

})();