;(function(){

var innerContainer = document.querySelector('.inner-container'),
	nextArticle = document.querySelector('.next-article');

window.addEventListener('load', function(){
	Velocity( innerContainer, { translateY: '-60px' }, { duration: 600 });
});

window.addEventListener('scroll', function(){
	if ((window.innerHeight + window.scrollY) >= (document.body.scrollHeight - 200)) {
		Velocity( nextArticle, { translateY: 0, opacity: 1 }, { duration: 750 });
	}
});

})();