;(function(){

var formModal = document.querySelector('form.contact-form-modal'),
	contactButton = document.querySelector('a.contact-button'),
	thankYouMessage = document.querySelector('.thank-you-message'),
	contactModal = document.querySelector('.contact-us-modal'),
	ballPulseSync = document.querySelector('.ball-pulse-sync'),
	modalTransformer = document.querySelector('.modal-transformer'),
	closeModal = document.querySelector('.close-modal');

formModal.addEventListener('submit', function(event){
	event.preventDefault();
	contactButton.style.pointerEvents = 'none';
	Velocity(ballPulseSync, 
		{opacity: 1}, 
		{visibility: 'visible'}, 
		{duration: 50});
	setTimeout(function(){
		Velocity(ballPulseSync, 
			{opacity: 0}, 
			{visibility: 'hidden'}, 
			{duration: 100}
		);
		contactButton.innerText = "Gracias";
		Velocity(formModal, {opacity: 0}, {duration: 200});
	}, 2400);
	setTimeout(function(){
		formModal.style.display = 'none';
		Velocity(thankYouMessage, 
			{opacity: 1, translateY: '-55px'}, 
			{visibility: 'visible'}, 
			{duration: 100}
		);
	}, 2800);
});

})();